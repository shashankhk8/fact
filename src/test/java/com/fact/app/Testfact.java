import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import com.fact.app.*;
public class Testfact{
	@Test
	void factorial(){
		assertEquals(120,Fact.Prod(5));
	}
	@Test
	void factorial1(){
		assertEquals(24,Fact.Prod(4));
	}
	@Test
	void factorial2(){
		assertEquals(6,Fact.Prod(3));
	}
	@Test
	void factorial3(){
		assertEquals(20,Fact.Prod(2));
	}
}


